/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/style.css":
/*!***********************!*\
  !*** ./src/style.css ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./style.css */ "./src/style.css");


const imageSlider = (() => {
  const TIME_INTERVAL = 5000;
  const arrowLeftEl = document.querySelector("[data-arrow-left]");
  const arrowRightEl = document.querySelector("[data-arrow-right]");
  let slidesEl = document.querySelectorAll(".slides img");
  let dotEls = document.getElementsByClassName("dot");
  let slideIndex = 0;
  let intervalId = null;

  const init = () => {
    eventBinder();
    renderSlide();
  };

  const renderSlide = () => {
    if (slidesEl.length > 0) {
      slidesEl[slideIndex].classList.add("displaySlide");
      intervalId = setInterval(nextSlide, TIME_INTERVAL);
    }
  };

  const dots = () => {
    for (let i = 0; i < dotEls.length; i++) {
      dotEls[i].addEventListener("click", () => {
        dotEls[i].className = dotEls[i].className.replace("active", "");
      });
    }
    dotEls[slideIndex - 1].className += "active";
  };

  const showSlide = (index) => {
    if (index >= slidesEl.length) {
      slideIndex = 0;
    } else if (index < 0) {
      slideIndex = slidesEl.length - 1;
    }

    slidesEl.forEach((slide) => {
      slide.classList.remove("displaySlide");
    });
    slidesEl[slideIndex].classList.add("displaySlide");
    dots();
  };

  const nextSlide = () => {
    slideIndex++;
    showSlide(slideIndex);
  };

  const prevSlide = () => {
    clearInterval(intervalId);
    slideIndex--;
    showSlide(slideIndex);
  };

  const eventBinder = () => {
    arrowLeftEl.addEventListener("click", (e) => {
      e.preventDefault();
      arrowClickHandler(arrowLeftEl);
    });
    arrowRightEl.addEventListener("click", (e) => {
      e.preventDefault();
      arrowClickHandler(arrowRightEl);
    });
  };

  const arrowClickHandler = (arrowEl) => {
    if (arrowEl == arrowLeftEl) {
      prevSlide();
    } else if (arrowEl == arrowRightEl) {
      nextSlide();
    }
  };

  return {
    init,
  };
})();

document.addEventListener("DOMContentLoaded", imageSlider.init());

})();

/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVuZGxlLjYwZjQ5ZDcxM2Q4ODNjNDA1MjU5LmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUE7Ozs7Ozs7VUNBQTtVQUNBOztVQUVBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBOztVQUVBO1VBQ0E7O1VBRUE7VUFDQTtVQUNBOzs7OztXQ3RCQTtXQUNBO1dBQ0E7V0FDQSx1REFBdUQsaUJBQWlCO1dBQ3hFO1dBQ0EsZ0RBQWdELGFBQWE7V0FDN0Q7Ozs7Ozs7Ozs7OztBQ05xQjs7QUFFckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxvQkFBb0IsbUJBQW1CO0FBQ3ZDO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBOztBQUVBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCIsInNvdXJjZXMiOlsid2VicGFjazovL21vYmlsZV9tZW51Ly4vc3JjL3N0eWxlLmNzcz8zZTNhIiwid2VicGFjazovL21vYmlsZV9tZW51L3dlYnBhY2svYm9vdHN0cmFwIiwid2VicGFjazovL21vYmlsZV9tZW51L3dlYnBhY2svcnVudGltZS9tYWtlIG5hbWVzcGFjZSBvYmplY3QiLCJ3ZWJwYWNrOi8vbW9iaWxlX21lbnUvLi9zcmMvaW5kZXguanMiXSwic291cmNlc0NvbnRlbnQiOlsiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luXG5leHBvcnQge307IiwiLy8gVGhlIG1vZHVsZSBjYWNoZVxudmFyIF9fd2VicGFja19tb2R1bGVfY2FjaGVfXyA9IHt9O1xuXG4vLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcblx0dmFyIGNhY2hlZE1vZHVsZSA9IF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF07XG5cdGlmIChjYWNoZWRNb2R1bGUgIT09IHVuZGVmaW5lZCkge1xuXHRcdHJldHVybiBjYWNoZWRNb2R1bGUuZXhwb3J0cztcblx0fVxuXHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuXHR2YXIgbW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXSA9IHtcblx0XHQvLyBubyBtb2R1bGUuaWQgbmVlZGVkXG5cdFx0Ly8gbm8gbW9kdWxlLmxvYWRlZCBuZWVkZWRcblx0XHRleHBvcnRzOiB7fVxuXHR9O1xuXG5cdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuXHRfX3dlYnBhY2tfbW9kdWxlc19fW21vZHVsZUlkXShtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuXHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuXHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG59XG5cbiIsIi8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbl9fd2VicGFja19yZXF1aXJlX18uciA9IChleHBvcnRzKSA9PiB7XG5cdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuXHR9XG5cdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG59OyIsImltcG9ydCBcIi4vc3R5bGUuY3NzXCI7XG5cbmNvbnN0IGltYWdlU2xpZGVyID0gKCgpID0+IHtcbiAgY29uc3QgVElNRV9JTlRFUlZBTCA9IDUwMDA7XG4gIGNvbnN0IGFycm93TGVmdEVsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIltkYXRhLWFycm93LWxlZnRdXCIpO1xuICBjb25zdCBhcnJvd1JpZ2h0RWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiW2RhdGEtYXJyb3ctcmlnaHRdXCIpO1xuICBsZXQgc2xpZGVzRWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiLnNsaWRlcyBpbWdcIik7XG4gIGxldCBkb3RFbHMgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKFwiZG90XCIpO1xuICBsZXQgc2xpZGVJbmRleCA9IDA7XG4gIGxldCBpbnRlcnZhbElkID0gbnVsbDtcblxuICBjb25zdCBpbml0ID0gKCkgPT4ge1xuICAgIGV2ZW50QmluZGVyKCk7XG4gICAgcmVuZGVyU2xpZGUoKTtcbiAgfTtcblxuICBjb25zdCByZW5kZXJTbGlkZSA9ICgpID0+IHtcbiAgICBpZiAoc2xpZGVzRWwubGVuZ3RoID4gMCkge1xuICAgICAgc2xpZGVzRWxbc2xpZGVJbmRleF0uY2xhc3NMaXN0LmFkZChcImRpc3BsYXlTbGlkZVwiKTtcbiAgICAgIGludGVydmFsSWQgPSBzZXRJbnRlcnZhbChuZXh0U2xpZGUsIFRJTUVfSU5URVJWQUwpO1xuICAgIH1cbiAgfTtcblxuICBjb25zdCBkb3RzID0gKCkgPT4ge1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZG90RWxzLmxlbmd0aDsgaSsrKSB7XG4gICAgICBkb3RFbHNbaV0uYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsICgpID0+IHtcbiAgICAgICAgZG90RWxzW2ldLmNsYXNzTmFtZSA9IGRvdEVsc1tpXS5jbGFzc05hbWUucmVwbGFjZShcImFjdGl2ZVwiLCBcIlwiKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgICBkb3RFbHNbc2xpZGVJbmRleCAtIDFdLmNsYXNzTmFtZSArPSBcImFjdGl2ZVwiO1xuICB9O1xuXG4gIGNvbnN0IHNob3dTbGlkZSA9IChpbmRleCkgPT4ge1xuICAgIGlmIChpbmRleCA+PSBzbGlkZXNFbC5sZW5ndGgpIHtcbiAgICAgIHNsaWRlSW5kZXggPSAwO1xuICAgIH0gZWxzZSBpZiAoaW5kZXggPCAwKSB7XG4gICAgICBzbGlkZUluZGV4ID0gc2xpZGVzRWwubGVuZ3RoIC0gMTtcbiAgICB9XG5cbiAgICBzbGlkZXNFbC5mb3JFYWNoKChzbGlkZSkgPT4ge1xuICAgICAgc2xpZGUuY2xhc3NMaXN0LnJlbW92ZShcImRpc3BsYXlTbGlkZVwiKTtcbiAgICB9KTtcbiAgICBzbGlkZXNFbFtzbGlkZUluZGV4XS5jbGFzc0xpc3QuYWRkKFwiZGlzcGxheVNsaWRlXCIpO1xuICAgIGRvdHMoKTtcbiAgfTtcblxuICBjb25zdCBuZXh0U2xpZGUgPSAoKSA9PiB7XG4gICAgc2xpZGVJbmRleCsrO1xuICAgIHNob3dTbGlkZShzbGlkZUluZGV4KTtcbiAgfTtcblxuICBjb25zdCBwcmV2U2xpZGUgPSAoKSA9PiB7XG4gICAgY2xlYXJJbnRlcnZhbChpbnRlcnZhbElkKTtcbiAgICBzbGlkZUluZGV4LS07XG4gICAgc2hvd1NsaWRlKHNsaWRlSW5kZXgpO1xuICB9O1xuXG4gIGNvbnN0IGV2ZW50QmluZGVyID0gKCkgPT4ge1xuICAgIGFycm93TGVmdEVsLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCAoZSkgPT4ge1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgYXJyb3dDbGlja0hhbmRsZXIoYXJyb3dMZWZ0RWwpO1xuICAgIH0pO1xuICAgIGFycm93UmlnaHRFbC5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgKGUpID0+IHtcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgIGFycm93Q2xpY2tIYW5kbGVyKGFycm93UmlnaHRFbCk7XG4gICAgfSk7XG4gIH07XG5cbiAgY29uc3QgYXJyb3dDbGlja0hhbmRsZXIgPSAoYXJyb3dFbCkgPT4ge1xuICAgIGlmIChhcnJvd0VsID09IGFycm93TGVmdEVsKSB7XG4gICAgICBwcmV2U2xpZGUoKTtcbiAgICB9IGVsc2UgaWYgKGFycm93RWwgPT0gYXJyb3dSaWdodEVsKSB7XG4gICAgICBuZXh0U2xpZGUoKTtcbiAgICB9XG4gIH07XG5cbiAgcmV0dXJuIHtcbiAgICBpbml0LFxuICB9O1xufSkoKTtcblxuZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIkRPTUNvbnRlbnRMb2FkZWRcIiwgaW1hZ2VTbGlkZXIuaW5pdCgpKTtcbiJdLCJuYW1lcyI6W10sInNvdXJjZVJvb3QiOiIifQ==